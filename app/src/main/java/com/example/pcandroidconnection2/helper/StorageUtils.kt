package com.example.pc_androidconnection2.helper

import android.os.Environment
import me.kacper.barszczewski.common.utils.LoggerWrapper
import java.io.*
import java.util.*

class StorageUtils {

    class StorageInfo(
        val path: String,
        val readonly: Boolean,
        val removable: Boolean,
        val number: Int
    ) {
        fun getDisplayName(): String {
            val stringBuilder = StringBuilder()
            if (!removable) {
                stringBuilder.append("Internal Storage")
            } else if (number > 1) {
                stringBuilder.append("SD card ").append(number)
            } else {
                stringBuilder.append("SD card")
            }
            if (readonly) {
                stringBuilder.append(" (Read only)")
            }
            return stringBuilder.toString()
        }
    }

    companion object {

        private val logger = LoggerWrapper(StorageUtils::class)

        fun getStorageList(): List<StorageInfo> {
            val list = mutableListOf<StorageInfo>()
            val defPath = Environment.getExternalStorageDirectory().path
            val defPathRemovable = Environment.isExternalStorageRemovable()
            val defPathState = Environment.getExternalStorageState()
            val defPathAvailable = defPathState.equals(Environment.MEDIA_MOUNTED)
                    || defPathState.equals(Environment.MEDIA_MOUNTED_READ_ONLY)
            val defPathReadOnly =
                Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED_READ_ONLY)

            val paths = mutableSetOf<String>()
            var curRemovableNumber = 1
            if (defPathAvailable) {
                paths.add(defPath)
                list.add(
                    0,
                    StorageInfo(
                        defPath,
                        defPathReadOnly,
                        defPathRemovable,
                        if (defPathRemovable) curRemovableNumber++ else -1
                    )
                )
            }

            var bufReader: BufferedReader? = null
            try {
                bufReader = BufferedReader(FileReader("/proc/mounts"))
                var line: String? = bufReader.readLine()
                while (line != null) {
                    logger.debug { line + "" }
                    val tokens = StringTokenizer(line, " ")
                    val mPath = tokens.nextToken() // device
                    val mountPoint = tokens.nextToken() // mount point
                    if (paths.contains(mountPoint)) {
                        line = bufReader.readLine()
                        continue
                    }
                    val unused = tokens.nextToken() // file system
                    val flags = tokens.nextToken().split(",") // flags
                    val readonly = flags.contains("ro")
                    if (readonly) {
                        line = bufReader.readLine()
                        continue
                    }

                    if (!line.contains("/mnt/secure")
                        && !line.contains("/mnt/asec")
                        && !line.contains("/mnt/obb")
                        && !line.contains("/dev/mapper")
                        && !line.contains("tmpfs")
                        && !mountPoint.startsWith("/proc")
                    ) {
                        val testFile = File(mountPoint)
                        if (testFile.isDirectory && testFile.listFiles() != null) {
                            paths.add(mountPoint)
                            list.add(StorageInfo(mountPoint, readonly, true, curRemovableNumber++))
                        }
                    }
                    line = bufReader.readLine()
                }
            } catch (ex: FileNotFoundException) {
                ex.printStackTrace()
            } catch (ex: IOException) {
                ex.printStackTrace()
            } finally {
                if (bufReader != null) {
                    try {
                        bufReader.close()
                    } catch (ex: IOException) {
                    }
                }
            }
            return list
        }

    }
}