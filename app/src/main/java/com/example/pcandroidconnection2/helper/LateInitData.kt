package com.example.pcandroidconnection2.helper


/**
 * Should be run only on background thread
 */
class LateInitData<T> {

    private val LOCKER = Object()

    var value: T? = null
        set(newValue) {
            synchronized(LOCKER) {
                field = newValue
            }
            if (newValue != null) {
                invokeObservers(newValue)
            }
        }

    private val observers: MutableList<(T) -> Unit> = mutableListOf()

    fun runWhenReady(observer: (T) -> Unit) {
        val cachedValue = synchronized(LOCKER) { value }
        if (cachedValue != null) {
            observer.invoke(cachedValue)
            return
        }
        synchronized(observers) {
            observers.add(observer)
        }
    }

    private fun invokeObservers(value: T) {
        synchronized(observers) {
            observers.forEach {
                it.invoke(value)
            }
            observers.clear()
        }
    }

    fun dispose() {
        synchronized(observers) {
            observers.clear()
        }
        synchronized(LOCKER) {
            value = null
        }
    }

}
