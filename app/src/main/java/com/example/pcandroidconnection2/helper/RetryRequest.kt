package com.example.pc_androidconnection2.helper

import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import me.kacper.barszczewski.entities.PathDiscoveryEntity
import me.kacper.barszczewski.entities.request.RequestPathDiscovery

private const val RETRY_DELAY = 5_000L

class RetryRequest(
//    val fragment: Fragment,
    val pathDiscovery: RequestPathDiscovery,
    val callback: (PathDiscoveryEntity) -> Unit
) {

    var snackbar: Snackbar? = null

    var isResponseReceived: Boolean = false

    var runnable: Runnable = Runnable { }

    private fun requestDelayCallback() {
        if (isResponseReceived) {
            return
        }
        //TODO FIx me
//        fragment.showSnackbarWithButton(
//            R.string.long_server_delay,
//            R.string.btn_retry,
//            retryRequesting()
//        )
    }

    fun invokeRequest(runnable: Runnable) {
        this.runnable = runnable
        runnable.run()
        CoroutineScope(Dispatchers.Main).launch {
            delay(RETRY_DELAY)
            requestDelayCallback()
        }
    }

    private fun retryRequesting(): View.OnClickListener = View.OnClickListener {
        if (!isResponseReceived) {
            invokeRequest(runnable)
        }
    }

    var safeCallback = fun(pathDiscoveryEntity: PathDiscoveryEntity) {
        isResponseReceived = true
        if (snackbar?.isShownOrQueued == true) {
            snackbar?.dismiss()
        }
        callback.invoke(pathDiscoveryEntity)
    }
        private set

}