package com.example.pc_androidconnection2.exceptions

import java.lang.RuntimeException

class AlreadyConnectedException(message: String) : RuntimeException(message)