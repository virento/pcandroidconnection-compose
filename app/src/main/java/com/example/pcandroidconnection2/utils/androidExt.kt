package com.example.pcandroidconnection2.utils

import android.content.Context
import com.example.pcandroidconnection2.di.CommonEntryPoint
import com.example.pcandroidconnection2.di.PcAndroidServiceModule
import dagger.hilt.android.EntryPointAccessors

fun entryPoint(context: Context) = EntryPointAccessors.fromApplication(context, CommonEntryPoint::class.java)