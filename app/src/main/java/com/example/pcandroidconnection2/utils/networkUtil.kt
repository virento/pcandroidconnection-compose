package com.example.pcandroidconnection2.utils

import java.net.Inet4Address
import java.net.NetworkInterface

fun getListOfBroadcastAddresses(): List<String> {
    val iterator = NetworkInterface.getNetworkInterfaces().iterator()
    val broadcastAddresses = mutableListOf<String>()
    while (iterator.hasNext()) {
        val network = iterator.next()
        if (network.isLoopback) {
            continue
        }
        val interfaceAddresses = network.interfaceAddresses
        broadcastAddresses.addAll(interfaceAddresses.filter {
            it.broadcast is Inet4Address
        }.map {
            it.broadcast.hostAddress
        })

    }
    return broadcastAddresses
}