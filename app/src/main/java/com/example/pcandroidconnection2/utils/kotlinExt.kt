package com.example.pcandroidconnection2.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch

suspend inline fun <T> Channel<T>.receiveWithoutError(): T? {
    return try {
        receive()
    } catch (e: Exception) {
        null
    }
}