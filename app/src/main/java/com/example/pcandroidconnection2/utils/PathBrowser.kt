package com.example.pcandroidconnection2.utils

import java.io.File

private const val DELIMITER_WINDOWS = '\\'

class PathBrowser(
    var actualPath: String = ""
) {

//    var actualPath = ""

    fun addNode(name: String): String {
        return File(actualPath, name).absolutePath
    }

    /**
     * returns null if there is no parent node
     */
    fun popNode(): String? {
        val delimiter = if (actualPath.contains("/")) "/" else "\\"
        var path: String? = actualPath
        if (delimiter == "\\") {
            path = (path as String).replace("\\", "/")
            path = File(path).parent
            if (path != null) path = path.replace("/", "\\") + "\\"
        } else {
            path = File(path as String).parent
        }
        return path
    }

    fun canPopNode(): Boolean = popNode() != null

}