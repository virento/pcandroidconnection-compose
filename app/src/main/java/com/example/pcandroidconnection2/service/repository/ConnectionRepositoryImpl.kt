package com.example.pcandroidconnection2.service.repository

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder
import androidx.lifecycle.MutableLiveData
import com.example.pc_androidconnection2.helper.RetryRequest
import com.example.pcandroidconnection2.helper.LateInitData
import com.example.pcandroidconnection2.nav.connection.entity.ConnectionData
import com.example.pcandroidconnection2.service.ConnectionJobStatus
import com.example.pcandroidconnection2.service.ConnectionRepository
import com.example.pcandroidconnection2.service.ConnectionService

class ConnectionRepositoryImpl(
    context: Context
) : ConnectionRepository {
    private var connectionService: LateInitData<ConnectionService> = LateInitData()
    private var bound = false

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            val binder = service as ConnectionService.ConnectionBinder
            connectionService.value = binder.getService()
            bound = true
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            bound = false
            connectionService.dispose()
        }
    }

    init {
        Intent(context, ConnectionService::class.java).also { intent ->
            context.bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }
    }

    override suspend fun connectToServer(
        connectionData: ConnectionData,
        finishStatus: MutableLiveData<ConnectionJobStatus>
    ) {
        connectionService.runWhenReady {
            connectionService.value?.connectToServer(connectionData,
                errorCallback = {
                    finishStatus.postValue(ConnectionJobStatus(true))
                },
                successCallback = {
                    finishStatus.postValue(ConnectionJobStatus(false))
                }
            )
        }
    }

    override suspend fun requestFilesInPath(request: RetryRequest) {
        val service = connectionService.value!!
        if (request.pathDiscovery.path.isEmpty()) {
            val discInfo = service.initializationInformation!!.homeDirectory
            request.pathDiscovery.path = discInfo.defaultPath
        }
        service.requestFilesInPath(request)
    }

    override fun disconnect() {
        connectionService.runWhenReady {
            it.disconnect()
        }
    }

}