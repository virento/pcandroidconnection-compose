package com.example.pcandroidconnection2.service

import androidx.lifecycle.MutableLiveData
import com.example.pc_androidconnection2.helper.RetryRequest
import com.example.pcandroidconnection2.nav.connection.entity.ConnectionData

interface ConnectionRepository {

    suspend fun connectToServer(connectionData: ConnectionData, finishStatus: MutableLiveData<ConnectionJobStatus>)

    suspend fun requestFilesInPath(request: RetryRequest)

    fun disconnect()

}