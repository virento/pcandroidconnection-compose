package com.example.pcandroidconnection2.service

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import androidx.core.os.HandlerCompat
import com.example.pc_androidconnection2.exceptions.AlreadyConnectedException
import com.example.pc_androidconnection2.helper.RetryRequest
import com.example.pcandroidconnection2.nav.connection.entity.ConnectionData
import com.github.aakira.napier.Napier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import me.kacper.barszczewski.common.*
import me.kacper.barszczewski.common.entities.FileTransferEntity
import me.kacper.barszczewski.common.entities.TransferInitializationEntity
import me.kacper.barszczewski.common.servers.ClientFileTransferListener
import me.kacper.barszczewski.entities.InitializationInformationEntity
import me.kacper.barszczewski.entities.PathDiscoveryEntity
import me.kacper.barszczewski.entities.request.RequestInitializationInformation
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ConnectionService : Service() {

    private var remoteClient: RemoteClient? = null

    private var isConnecting = false

    private val connectionBinder = ConnectionBinder()

    private val pathCallbacks = mutableMapOf<String, (PathDiscoveryEntity) -> Unit>()

    val executorService: ExecutorService = Executors.newFixedThreadPool(4)
    val mainThreadHandler: Handler = HandlerCompat.createAsync(Looper.getMainLooper())

    var initializationInformationLocker = Any()
    var initializationInformation: InitializationInformationEntity? = null
        private set
    var initializationInformationCallback: () -> Unit = { }

    inner class ConnectionBinder : Binder() {
        fun getService(): ConnectionService {
            return this@ConnectionService
        }
    }

    fun connectToServer(
        connectionData: ConnectionData,
        successCallback: (RemoteClient) -> Unit,
        errorCallback: () -> Unit
    ) {
        if (remoteClient != null) {
            throw AlreadyConnectedException("Connection is already running!")
        }
        synchronized(isConnecting) {
            if (isConnecting) {
                return
            }
            isConnecting = true
            RemoteConnection.connectToServer(
                connectionData.hostname,
                connectionData.port,
                callback = {
                    this.remoteClient = it; isConnecting = false; successCallback.invoke(it)
                    addMessagesHandlers(it)
                    it.addClientInitializedListener { requestInitializationInformationFromServer(it) }
                    it.addListener(ClientListenerType.CLOSED) { client -> client.listeners[ClientListenerType.FILE_TRANSFER]?.clear() }
                    it.thread.start()
                },
                errorCallback = {
                    this.remoteClient = null; isConnecting = false; errorCallback.invoke()
                }
            )
        }
    }

    private fun addMessagesHandlers(remoteClient: RemoteClient) {
        remoteClient.addMessageHandler(
            InitializationInformationEntity.CODE,
            initializationInformationHandler
        )
        remoteClient.addMessageHandler(PathDiscoveryEntity.CODE) {
            val pathEntity = it.deserialize<PathDiscoveryEntity>()
            if (pathCallbacks.containsKey(pathEntity.path)) {
                mainThreadHandler.post { pathCallbacks[pathEntity.path]!!.invoke(pathEntity) }
            }
        }
    }

    private val initializationInformationHandler = fun(messageData: MessageData) {
        synchronized(initializationInformationLocker) {
            initializationInformation = messageData.deserialize()
        }
        initializationInformationCallback.invoke()
    }

    fun setInitializationCallback(callback: () -> Unit) {
        initializationInformationCallback = callback
    }

    private fun requestInitializationInformationFromServer(remoteClient: RemoteClient) {
        executorService.execute {
            remoteClient.sendMessage(RequestInitializationInformation(true))
        }
    }

    public fun requestFileTransfer(fileEntity: TransferInitializationEntity, clientFileTransferListener: ClientFileTransferListener) {
        executorService.execute {
            remoteClient?.sendMessage(fileEntity)
        }
        addFileTransferListener(clientFileTransferListener)
    }

    public fun addFileTransferListener(clientFileTransferListener: ClientFileTransferListener) {
        remoteClient?.addListener(ClientListenerType.FILE_TRANSFER) {
            it.fileClient?.addTransferListener(clientFileTransferListener)
        }
    }

//    fun requestFilesInPath(path: String, callback: (PathDiscoveryEntity) -> Unit) {
//        pathCallbacks[path] = callback
//
//        executorService.execute {
//            (remoteClient as RemoteClient).sendMessage(RequestPathDiscovery(path))
//        }
//    }

    fun requestFilesInPath(request: RetryRequest) {
        request.invokeRequest {
            pathCallbacks[request.pathDiscovery.path] = request.safeCallback

            executorService.execute { // TODO remove executor, invoke within suspended method
                (remoteClient as RemoteClient).sendMessage(request.pathDiscovery)
            }
        }
    }

    fun disconnect() {
        CoroutineScope(Dispatchers.IO).launch {
            try {
                remoteClient?.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }

            // In case, check if client thread is closed
            remoteClient?.let { if (!it.closed) it.close() }
        }.invokeOnCompletion {
            if (it != null) {
                Napier.e("Error when disconnecting from server", it)
            }
            remoteClient = null
        }
    }

    override fun onBind(intent: Intent): IBinder {
        return connectionBinder
    }

    override fun onDestroy() {
        remoteClient?.close()
        super.onDestroy()
    }

}