package com.example.pcandroidconnection2.service

data class ConnectionJobStatus(
    val failed: Boolean
)