package com.example.pcandroidconnection2.service

import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.pcandroidconnection2.nav.connection.entity.ConnectionData
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ConnectionViewModel @Inject constructor() : ViewModel() {

    var connectionData: ConnectionData? = null

    val connectingStatus = mutableStateOf(false)

}