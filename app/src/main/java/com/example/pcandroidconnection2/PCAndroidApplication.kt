package com.example.pcandroidconnection2

import android.app.Application
import com.example.pcandroidconnection2.nav.connection.repository.DiscoveryRepository
import com.example.pcandroidconnection2.nav.connection.repository.impl.DiscoveryRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.components.SingletonComponent

@HiltAndroidApp
class PCAndroidApplication : Application() {
}