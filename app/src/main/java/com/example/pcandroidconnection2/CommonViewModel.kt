package com.example.pcandroidconnection2

import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.scopes.ActivityRetainedScoped
import javax.inject.Inject

@HiltViewModel
class CommonViewModel @Inject constructor() : ViewModel() {

    lateinit var navController: NavController
    var toolbarTitle = mutableStateOf("PC Android Connection")

    var toolbarLeftPad = 5

}