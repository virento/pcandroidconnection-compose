package com.example.pcandroidconnection2.ext

import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.layout

fun Modifier.snipPath() = layout { measurable, constraints ->
    val placeable = measurable.measure(constraints)
    layout(placeable.width, placeable.height) {
        placeable.placeRelative(0, 0)
    }
}