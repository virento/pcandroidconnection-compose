package com.example.pcandroidconnection2

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideOutHorizontally
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.layout.SubcomposeLayout
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.LocalViewModelStoreOwner
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.navigation.animation.composable
import com.example.pcandroidconnection2.nav.action.Action
import com.example.pcandroidconnection2.nav.browser.pc.PcBrowser
import com.example.pcandroidconnection2.nav.connection.ConnectionSelect
import com.example.pcandroidconnection2.ui.theme.PcAndroidConnection2Theme
import com.github.aakira.napier.DebugAntilog
import com.github.aakira.napier.Napier
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import dagger.hilt.android.AndroidEntryPoint
import me.kacper.barszczewski.entities.SerializationModule

object NavigationPath {
    const val ACTION = "action"
    const val CONNECTION_SELECT = "connectionSelect"
    const val PC_BROWSER = "pcBrowser"
    const val SUMMARY = "summary"
}

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    @ExperimentalAnimationApi
    @ExperimentalMaterialApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Napier.base(DebugAntilog())
        SerializationModule.register()
        setContent {
            PcAndroidConnection2Theme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    MasterView()
                }
            }
        }
    }
}

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun MasterView(
    commonModel: CommonViewModel = viewModel(),
) {
    val navController = rememberAnimatedNavController()
    commonModel.navController = navController

    val viewModelStoreOwner = checkNotNull(LocalViewModelStoreOwner.current) {
        "No ViewModelStoreOwner was provided via LocalViewModelStoreOwner"
    }
    Scaffold(
        topBar = { Toolbar(commonModel) },
        content = {
            AnimatedNavHost(navController = navController, startDestination = NavigationPath.CONNECTION_SELECT) {
                composable(
                    NavigationPath.CONNECTION_SELECT,
                    exitTransition = { initial, _ ->
                        slideOutHorizontally(targetOffsetX = { -1000 }, animationSpec = tween(700))
                    }
                ) {
                    CompositionLocalProvider(LocalViewModelStoreOwner provides viewModelStoreOwner) {
                        ConnectionSelect()
                    }
                }
                composable(NavigationPath.ACTION) {
                    CompositionLocalProvider(
                        LocalViewModelStoreOwner provides viewModelStoreOwner
                    ) {
                        Action()
                    }
                }
                composable(NavigationPath.PC_BROWSER) {
                    CompositionLocalProvider(LocalViewModelStoreOwner provides viewModelStoreOwner) {
                        PcBrowser()
                    }
                }
                composable(NavigationPath.SUMMARY) {
                    // TODO
                }
            }
        }
    )
}

@Composable
fun Toolbar(commonModel: CommonViewModel) {
    var toolbarTitleRemember by remember { commonModel.toolbarTitle }
    TopAppBar(title = {
        var finalText by remember { mutableStateOf("") }
        var wasOverflowed = false
        SubcomposeLayout { constraints ->
            subcompose("measureText") {
                Text(toolbarTitleRemember, overflow = TextOverflow.Visible, maxLines = 1, onTextLayout = { layout ->
                    if (layout.hasVisualOverflow) {
                        toolbarTitleRemember = toolbarTitleRemember.substring(0, commonModel.toolbarLeftPad) + toolbarTitleRemember.substring(commonModel.toolbarLeftPad + 1)
                        wasOverflowed = true
                    } else {
                        var testFixed = toolbarTitleRemember;
                        if (wasOverflowed) {
                            testFixed = toolbarTitleRemember.substring(0, commonModel.toolbarLeftPad) + "..." + toolbarTitleRemember.substring(4 + commonModel.toolbarLeftPad)
                        }
                        finalText = testFixed
                        wasOverflowed = false
                    }
                })
            }[0].measure(constraints).width
            val contentPlaceable = subcompose("content") {
                Text(finalText, overflow = TextOverflow.Visible, maxLines = 1)
            }[0].measure(constraints)
            layout(constraints.maxWidth, constraints.maxHeight) {
                contentPlaceable.place(0, (constraints.maxHeight - contentPlaceable.height)/2)
            }
        }
    })
}


@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    PcAndroidConnection2Theme {
        MasterView()
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun PreviewToolbar() {
    val viewModel = CommonViewModel()
    viewModel.toolbarTitle.value = "C:/super/long/path/thath/doesnt/fit/here32131231312"
    Scaffold(topBar = { Toolbar(viewModel) }) {

    }
}