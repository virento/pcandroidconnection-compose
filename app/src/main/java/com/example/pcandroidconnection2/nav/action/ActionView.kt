package com.example.pcandroidconnection2.nav.action

import android.content.Context
import android.content.Intent
import androidx.activity.compose.BackHandler
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults.buttonColors
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.pcandroidconnection2.CommonViewModel
import com.example.pcandroidconnection2.NavigationPath
import com.example.pcandroidconnection2.R
import com.example.pcandroidconnection2.nav.browser.android.AndroidBrowserViewModel
import com.example.pcandroidconnection2.service.ConnectionViewModel
import com.example.pcandroidconnection2.ui.theme.PcAndroidConnection2Theme
import com.example.pcandroidconnection2.utils.entryPoint

@ExperimentalAnimationApi
@Composable
fun Action(
    connectionModel: ConnectionViewModel = hiltViewModel(),
    commonModel: CommonViewModel = hiltViewModel()
) = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    modifier = Modifier
        .fillMaxWidth()
        .padding(8.dp, 8.dp)
        .padding(top = 32.dp)
        .fillMaxHeight()
) {
    val openDialog = remember { mutableStateOf(false) }
    val supportedBrowser = supportBrowser()

    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(24.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxWidth(0.9f)
    ) {
        item {
            ActionButton(buttonTitle = "Browse Android files") {

            }
        }
        item {
            if (!isSupportedBrowserResolved()) {
                DisabledActionButton(buttonTitle = "Use easy-to-use file browser") {
                    openDialog.value = true
                }
            } else {
                ActionButton(buttonTitle = "Use easy-to-use file browser", enabled = isSupportedBrowserResolved()) {
                    val chooseFile = supportedBrowserIntent()
                    supportedBrowser.launch(chooseFile)
                }
            }
        }
        item {
            ActionButton(buttonTitle = "Browse PC files") {
                commonModel.navController.navigate(NavigationPath.PC_BROWSER)
            }
        }
    }
    Column(
        verticalArrangement = Arrangement.Bottom,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.fillMaxHeight()
    ) {
        DisconnectButton()
    }

    if (openDialog.value) {
        ShowNotSupportDialog(openDialog)
    }
}

private fun supportedBrowserIntent(): Intent {
    val chooseFile = Intent(Intent.ACTION_GET_CONTENT)
    chooseFile.addCategory(Intent.CATEGORY_OPENABLE)
    chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
    chooseFile.type = "*/*"
    return chooseFile
}

@Composable
private fun isSupportedBrowserResolved(): Boolean {
    val chooseFile = Intent(Intent.ACTION_GET_CONTENT)
    chooseFile.addCategory(Intent.CATEGORY_OPENABLE)
    chooseFile.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
    chooseFile.type = "*/*"
    val context = LocalContext.current
    return chooseFile.resolveActivity(context.packageManager) != null
}

@Composable
fun ShowNotSupportDialog(openDialog: MutableState<Boolean>) =
    AlertDialog(
        onDismissRequest = {
            openDialog.value = false
        },
        title = { Text(stringResource(id = R.string.app_name)) },
        text = { Text(stringResource(id = R.string.application_not_found)) },
        confirmButton = {
            Button(onClick = { openDialog.value = false }) {
                Text(stringResource(id = android.R.string.ok))
            }
        }
    )

@Composable
private fun supportBrowser(
    commonModel: CommonViewModel = hiltViewModel(),
    androidBrowserViewModel: AndroidBrowserViewModel = hiltViewModel()
): ManagedActivityResultLauncher<Intent, ActivityResult> {
    return rememberLauncherForActivityResult(
        contract = ActivityResultContracts.StartActivityForResult(),
        onResult = {
            val numOfItems = it.data?.clipData?.itemCount ?: 0
            for (i in 0 until numOfItems) {
                it.data?.clipData?.getItemAt(i)?.let { androidBrowserViewModel.addFile(it.uri) }
            }
            if (numOfItems == 0) {
                commonModel.navController.navigate(NavigationPath.ACTION)
            } else {
                commonModel.navController.navigate(NavigationPath.SUMMARY)
            }
        }
    )
}

@Composable
fun DisconnectButton(
    commonModel: CommonViewModel = hiltViewModel(),
    connectionModel: ConnectionViewModel = hiltViewModel()
) {
    val context = LocalContext.current

    Button(
        onClick = {
            disconnectFromServer(context, commonModel, connectionModel)
//            commonModel.navController.popBackStack()
        },
        modifier = Modifier
            .fillMaxWidth(),
        colors = buttonColors(
            backgroundColor = MaterialTheme.colors.secondary
        )
    ) {
        Text(
            text = "Disconnect",
            modifier = Modifier.padding(
                bottom = 8.dp,
                top = 8.dp
            )
        )
    }
    BackHandler(connectionModel.connectingStatus.value) {
        disconnectFromServer(context, commonModel, connectionModel)
    }
}

private fun disconnectFromServer(
    context: Context,
    commonModel: CommonViewModel,
    connectionModel: ConnectionViewModel
) {
    entryPoint(context).connectionRepository().disconnect()
    connectionModel.connectingStatus.value = false
    commonModel.navController.popBackStack()
}

@Composable
fun ActionButton(
    buttonTitle: String,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    action: () -> Unit
) = Button(
    onClick = action,
    shape = RoundedCornerShape(4.dp),
    modifier = modifier.fillMaxWidth(),
    enabled = enabled
)
{
    Text(
        text = buttonTitle,
        modifier = Modifier.padding(
            top = 12.dp,
            bottom = 12.dp
        )
    )
}

@Composable
fun DisabledActionButton(
    buttonTitle: String,
    modifier: Modifier = Modifier,
    action: () -> Unit
) = DisabledButton(
    onClick = action,
    shape = RoundedCornerShape(4.dp),
    modifier = modifier.fillMaxWidth()
)
{
    Text(
        text = buttonTitle,
        modifier = Modifier.padding(
            top = 12.dp,
            bottom = 12.dp
        )
    )
}

/**
 * From Button.kt
 */
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun DisabledButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    elevation: ButtonElevation? = ButtonDefaults.elevation(),
    shape: Shape = MaterialTheme.shapes.small,
    border: BorderStroke? = null,
    colors: ButtonColors = ButtonDefaults.buttonColors(),
    contentPadding: PaddingValues = ButtonDefaults.ContentPadding,
    content: @Composable RowScope.() -> Unit
) {
    val enabled = true
    val contentColor by colors.contentColor(false)
    Surface(
        modifier = modifier,
        shape = shape,
        color = colors.backgroundColor(false).value,
        contentColor = contentColor.copy(alpha = 1f),
        border = border,
        elevation = elevation?.elevation(false, interactionSource)?.value ?: 0.dp,
        onClick = onClick,
        enabled = enabled,
        role = Role.Button,
        interactionSource = interactionSource,
        indication = rememberRipple()
    ) {
        CompositionLocalProvider(LocalContentAlpha provides contentColor.alpha) {
            ProvideTextStyle(
                value = MaterialTheme.typography.button
            ) {
                Row(
                    Modifier
                        .defaultMinSize(
                            minWidth = ButtonDefaults.MinWidth,
                            minHeight = ButtonDefaults.MinHeight
                        )
                        .padding(contentPadding),
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically,
                    content = content
                )
            }
        }
    }
}


/**
 * Previews
 */

@ExperimentalAnimationApi
@Preview(
    showSystemUi = true,
    showBackground = true
)
@Composable
fun PreviewAction() {
    PcAndroidConnection2Theme {
        Action()
    }
}
