package com.example.pcandroidconnection2.nav.connection.repository.impl

import androidx.compose.runtime.snapshots.SnapshotStateList
import com.example.pcandroidconnection2.nav.connection.entity.ConnectionData
import com.example.pcandroidconnection2.nav.connection.repository.DiscoveryRepository
import com.example.pcandroidconnection2.utils.getListOfBroadcastAddresses
import com.example.pcandroidconnection2.utils.receiveWithoutError
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.isActive
import kotlinx.coroutines.withContext
import me.kacper.barszczewski.common.client.DiscoveryClient
import javax.inject.Inject

class DiscoveryRepositoryImpl @Inject constructor() : DiscoveryRepository {

    override suspend fun discover(discoveredApp: SnapshotStateList<ConnectionData>) {
        val discoveryClient = DiscoveryClient(-1, destAddress = getListOfBroadcastAddresses())
        withContext(Dispatchers.IO) {
            val sendRequest = discoveryClient.sendRequest()
            while (this.isActive) {
                sendRequest.receiveWithoutError()?.let {
                    discoveredApp.add(
                        ConnectionData(
                            it.appName,
                            it.hostname.hostAddress,
                            it.port
                        )
                    )
                }
            }
            discoveryClient.stop()
        }
    }
}