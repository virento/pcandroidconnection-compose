package com.example.pcandroidconnection2.nav.connection.entity

import androidx.compose.animation.core.MutableTransitionState

data class ConnectionData(
    val name: String,
    val hostname: String,
    val port: Int
) {

    var visible = MutableTransitionState(false).apply { targetState = true }

}