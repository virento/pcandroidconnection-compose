package com.example.pcandroidconnection2.nav.browser.android

import android.net.Uri
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class AndroidBrowserViewModel @Inject constructor() : ViewModel() {

    private var _selectedFiles = SnapshotStateList<Uri>()
    var selectedFiles: List<Uri> = _selectedFiles

    fun addFile(uri: Uri) = _selectedFiles.add(uri)

}