package com.example.pcandroidconnection2.nav.connection

import androidx.compose.animation.*
import androidx.compose.animation.core.tween
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.pcandroidconnection2.CommonViewModel
import com.example.pcandroidconnection2.NavigationPath
import com.example.pcandroidconnection2.R
import com.example.pcandroidconnection2.nav.connection.entity.ConnectionData
import com.example.pcandroidconnection2.nav.connection.repository.DiscoveryRepository
import com.example.pcandroidconnection2.service.ConnectionJobStatus
import com.example.pcandroidconnection2.service.ConnectionRepository
import com.example.pcandroidconnection2.service.ConnectionViewModel
import com.example.pcandroidconnection2.ui.theme.PcAndroidConnection2Theme
import com.example.pcandroidconnection2.utils.entryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun ConnectionSelect(
    discoveryModel: DiscoveryViewModel = hiltViewModel()
) {
    val data = discoveryModel.discoveredApp
    LaunchedEffect(Unit) {
        discoveryModel.fadeIn()
    }
    Crossfade(targetState = data.isEmpty(), animationSpec = tween(1000)) { screen ->
        if (screen) {
            ShowSearchingForAppIndicator()
        } else {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .fillMaxHeight()
                    .padding(8.dp, 8.dp)
                    .fillMaxWidth()
            ) {
                Text(
                    "Choose connection",
                    fontSize = 24.sp,
                    modifier = Modifier.padding(top = 16.dp)
                )
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp, 16.dp)
                        .fillMaxHeight()
                ) {
                    ShowDiscoveredAppList(data)
                }
            }

        }
    }
}

@Composable
fun ShowSearchingForAppIndicator() = Column(
    modifier = Modifier
        .fillMaxWidth()
        .fillMaxHeight(),
    verticalArrangement = Arrangement.Center,
    horizontalAlignment = Alignment.CenterHorizontally
) {
    Text(
        text = "Searching for connections",
        fontSize = 30.sp,
        modifier = Modifier.padding(bottom = 8.dp)
    )
    CircularProgressIndicator(
        strokeWidth = 4.dp
    )
}

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun ShowDiscoveredAppList(data: List<ConnectionData>) {
    LazyColumn {
        itemsIndexed(data.toList()) { i, connection ->
            AnimatedVisibility(
                visibleState = connection.visible,
                enter = slideInHorizontally(
                    initialOffsetX = { it / 2 }
                ) + fadeIn(),
                exit = slideOutHorizontally() + fadeOut()
            ) {
                ShowDiscoverAppRow(connection)
            }
        }
    }
}
//@ExperimentalAnimationApi
//@ExperimentalMaterialApi
//@Composable
//fun ShowDiscoveredAppList(data: List<ConnectionData>) {
//    LazyColumn {
//        items(data.toList()) {
//            ShowDiscoverAppRow(it)
//        }
//    }
//}

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Composable
fun ShowDiscoverAppRow(
    data: ConnectionData,
    commonModel: CommonViewModel = viewModel(),
    connectionModel: ConnectionViewModel = hiltViewModel(),
    discoveryModel: DiscoveryViewModel = hiltViewModel()
) {
    val context = LocalContext.current
    var isConnecting by remember { mutableStateOf(false) }
    val isConnected: MutableLiveData<ConnectionJobStatus> = MutableLiveData()
    val scope = rememberCoroutineScope()

    val isConnectedState = isConnected.observeAsState()
    if (isConnectedState.value != null && !isConnectedState.value!!.failed) {
        discoveryModel.fadeOut()
        connectionModel.connectionData = data
        connectionModel.connectingStatus.value = true
        commonModel.navController.navigate(NavigationPath.ACTION)
    }

    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 16.dp)
            .clickable {
                scope.launch {
                    // Delay connecting for better visual effects, see @ProgressIndicator
                    delay(500)
                    entryPoint(context)
                        .connectionRepository()
                        .connectToServer(data, isConnected)
                }
                isConnecting = true
            },
        border = BorderStroke(1.dp, Color.LightGray),
        shape = RoundedCornerShape(13.dp),
        elevation = 12.dp
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            AnimatedContent(
                targetState = isConnecting,
                transitionSpec = {
                    (slideInVertically({ height -> 2 * height }) + fadeIn()
                            with slideOutVertically({ height -> -height }) + fadeOut()
                            using SizeTransform(clip = false))
                }
            ) { connecting ->
                if (connecting) {
                    CircularProgressIndicator(
                        modifier = Modifier.padding(start = 8.dp)
                    )
                } else {
                    Icon(
                        painter = painterResource(R.drawable.ic_baseline_devices_24),
                        contentDescription = "device icon",
                        modifier = Modifier
                            .width(48.dp)
                            .height(48.dp)
                            .padding(start = 8.dp)
                    )
                }
            }
            Column(
                modifier = Modifier.padding(16.dp)
            ) {
                Text(
                    data.name,
                    fontSize = 24.sp,
                    modifier = Modifier
                        .padding(bottom = 4.dp)
                        .animateContentSize()
                )
                Text("${data.hostname}:${data.port}", color = Color.Gray)
            }
        }
    }
}

@ExperimentalAnimationApi
@ExperimentalMaterialApi
@Preview(
    showSystemUi = true,
    showBackground = true
)
@Composable
fun PreviewConnectionSelect() {
    PcAndroidConnection2Theme {
        ConnectionSelect(discoveryModel = DiscoveryViewModel(
            object : DiscoveryRepository {
                override suspend fun discover(discoveredApp: SnapshotStateList<ConnectionData>) {
                    var i = 0
                    withContext(Dispatchers.IO) {
//                        while (i++ < 10) {
                        delay(2_000)
                        discoveredApp.add(ConnectionData("Test $i", "127.0.0.1", 50))
//                        }
                    }
//                    discoveredApp.postValue(mutableSetOf())
//                    discoveredApp.value = mutableSetOf()
//                    discoveredApp.value?.add(ConnectionData("Test", "127.0.0.1", 50))
//                    discoveredApp.notifyObserver()
                }
            }
        ))
    }
}

//@ExperimentalMaterialApi
//@ExperimentalAnimationApi
//@Preview
//@Composable
//fun PreviewDiscoveredAppCard() {
//    ShowDiscoverAppRow(
//        ConnectionData("Test", "127.0.0.1", 50),
//        connectionModel = ConnectionViewModel(object : ConnectionRepository {
//            override suspend fun connectToServer(
//                connectionData: ConnectionData,
//                finishStatus: MutableLiveData<ConnectionJobStatus>
//            ) {
//            }
//
//            override fun disconnect() {}
//        }),
//        commonModel = CommonViewModel()
//    )
//}