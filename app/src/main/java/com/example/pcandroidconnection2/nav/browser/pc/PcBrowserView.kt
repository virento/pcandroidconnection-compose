package com.example.pcandroidconnection2.nav.browser.pc

import androidx.activity.compose.BackHandler
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Checkbox
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.MutableLiveData
import com.example.pc_androidconnection2.helper.RetryRequest
import com.example.pcandroidconnection2.CommonViewModel
import com.example.pcandroidconnection2.R
import com.example.pcandroidconnection2.nav.browser.FileInfo
import com.example.pcandroidconnection2.nav.connection.entity.ConnectionData
import com.example.pcandroidconnection2.service.ConnectionJobStatus
import com.example.pcandroidconnection2.service.ConnectionRepository
import com.example.pcandroidconnection2.ui.theme.PcAndroidConnection2Theme
import com.example.pcandroidconnection2.utils.PathBrowser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import me.kacper.barszczewski.entities.PathDiscoveryEntity

@Composable
fun PcBrowser(
    commonViewModel: CommonViewModel = hiltViewModel(),
    browserViewModel: PcBrowserViewModel = hiltViewModel()
) {
    if (browserViewModel.currentPath() == "") {
        browserViewModel.newPath("")
    }
    BackHandler(PathBrowser(browserViewModel.currentPath()).canPopNode()) {
        browserViewModel.newPath(PathBrowser(browserViewModel.currentPath()).popNode()!!)
    }

    val data = browserViewModel.filesList
    val titleRemember by remember { browserViewModel.currentPath }
    if (titleRemember.isNotBlank()) {
        commonViewModel.toolbarTitle.value = titleRemember
    }

    LazyColumn {
        itemsIndexed(data) { i, fileInfo ->
            ShowFileInfoRow(fileInfo = fileInfo)
        }
    }
}

@Composable
fun ShowFileInfoRow(
    browserViewModel: PcBrowserViewModel = hiltViewModel(),
    fileInfo: FileInfo
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .padding(
                start = 8.dp,
                end = 8.dp
            )
            .fillMaxWidth()
            .clickable {
                browserViewModel.newPath(fileInfo)
            }
    ) {
        Row(
            modifier = Modifier
                .padding(start = 0.dp, end = 16.dp)
                .height(48.dp)
//                .fillMaxWidth()
        ) {
            if (fileInfo.directory) {
                Icon(
                    painter = painterResource(R.drawable.ic_baseline_folder_24),
                    contentDescription = "folder icon",
                    modifier = Modifier
                        .width(48.dp)
                        .height(48.dp)
                        .fillMaxHeight()
                )
            }
            Text(
                fileInfo.fileName,
                modifier = Modifier
                    .padding(bottom = 4.dp)
                    .animateContentSize()
                    .fillMaxWidth()
                    .align(Alignment.CenterVertically),
                fontSize = 24.sp
            )
            Checkbox(
                modifier = Modifier
                    .padding(end = 0.dp)
                    .align(Alignment.CenterVertically),
                checked = false,
                onCheckedChange = {}
            )
        }
    }
}


@Preview
@Composable
fun PreviewList() {
    val data = listOf(
        FileInfo("/", "Dictionary 1", true, false),
        FileInfo("/", "Dictionary 2", true, false),
        FileInfo("/", " File 1", false, false),
        FileInfo("/", " File 2", false, false),
        FileInfo("/", "Dictionary 2", true, false)
    )
    LazyColumn {
        itemsIndexed(data) { i, fileInfo ->
            ShowFileInfoRow(fileInfo = fileInfo)
        }
    }
}


