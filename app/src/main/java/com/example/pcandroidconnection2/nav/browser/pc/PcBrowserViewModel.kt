package com.example.pcandroidconnection2.nav.browser.pc

import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pc_androidconnection2.helper.RetryRequest
import com.example.pcandroidconnection2.nav.browser.FileInfo
import com.example.pcandroidconnection2.service.ConnectionRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import me.kacper.barszczewski.entities.request.RequestPathDiscovery
import javax.inject.Inject


@HiltViewModel
class PcBrowserViewModel @Inject constructor(
    private val connectionRepository: ConnectionRepository
) : ViewModel() {

    private var _pathDiscoveryEntity: RequestPathDiscovery = RequestPathDiscovery("")
    val currentPath = mutableStateOf("")

    private val _filesList = SnapshotStateList<FileInfo>()
    val filesList: List<FileInfo> = _filesList

    fun newPath(fileInfo: FileInfo) = newPath(fileInfo.getAbsolutePath())

    fun newPath(path: String) = viewModelScope.launch {
        _pathDiscoveryEntity.path = path
        connectionRepository.requestFilesInPath(RetryRequest(
            _pathDiscoveryEntity,
            callback =  {
                currentPath.value = _pathDiscoveryEntity.path
                val values = mutableListOf<FileInfo>()
                it.directories.forEach { driName ->
                    values.add(
                        FileInfo(
                            _pathDiscoveryEntity.path,
                            driName,
                            true
                        )
                    )
                }
                it.files.forEach { fileName ->
                    values.add(
                        FileInfo(
                            _pathDiscoveryEntity.path,
                            fileName,
                            false
                        )
                    )
                }
                _filesList.clear()
                _filesList.addAll(values)
            }
        ))
    }

    fun currentPath() : String = _pathDiscoveryEntity.path

}