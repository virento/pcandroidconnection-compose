package com.example.pcandroidconnection2.nav.connection

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pcandroidconnection2.nav.connection.entity.ConnectionData
import com.example.pcandroidconnection2.nav.connection.repository.DiscoveryRepository
import com.example.pcandroidconnection2.nav.connection.repository.impl.DiscoveryRepositoryImpl
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DiscoveryViewModel @Inject constructor(
    private val discoveryRepository: DiscoveryRepository
) : ViewModel() {

//    private val _discoveredApp = MutableLiveData<MutableSet<ConnectionData>>()
    private val _discoveredApp = SnapshotStateList<ConnectionData>()
    val discoveredApp: List<ConnectionData> = _discoveredApp

    var _runner: Job? = null

    init {
//        _runner = viewModelScope.launch {
////            _discoveredApp.value = mutableSetOf(ConnectionData("Test", "127.0.0.1", 50))
//            discoveryRepository.discover(_discoveredApp)
//        }
    }

    fun fadeIn() {
        _discoveredApp.clear()
        _runner?.cancel("New instance required")
        _runner = viewModelScope.launch {
//            _discoveredApp.value = mutableSetOf(ConnectionData("Test", "127.0.0.1", 50))
            discoveryRepository.discover(_discoveredApp)
        }
    }

    fun fadeOut() {
        _runner?.cancel("Fade out")
    }
}