package com.example.pcandroidconnection2.nav.browser

data class FileInfo(val path: String, val fileName: String, val directory: Boolean, val special: Boolean = false) {
    fun getAbsolutePath(): String {
        var delimiter = "\\"
        if (path.contains("/")) {
            delimiter = "/"
        }
        var absolutePath = path
        if (!path.endsWith(delimiter)) {
            absolutePath += delimiter
        }
        return absolutePath + fileName
    }
}