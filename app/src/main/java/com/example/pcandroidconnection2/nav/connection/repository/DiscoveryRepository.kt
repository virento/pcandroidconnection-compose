package com.example.pcandroidconnection2.nav.connection.repository

import androidx.compose.runtime.snapshots.SnapshotStateList
import com.example.pcandroidconnection2.nav.connection.entity.ConnectionData

interface DiscoveryRepository {

    suspend fun discover(discoveredApp: SnapshotStateList<ConnectionData>)

}