package com.example.pcandroidconnection2.di

import android.content.ContentProvider
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.example.pcandroidconnection2.nav.connection.repository.DiscoveryRepository
import com.example.pcandroidconnection2.nav.connection.repository.impl.DiscoveryRepositoryImpl
import com.example.pcandroidconnection2.service.repository.ConnectionRepositoryImpl
import com.example.pcandroidconnection2.service.ConnectionRepository
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.EntryPoint
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import dagger.hilt.android.components.ServiceComponent
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoMap
import javax.inject.Singleton

@Module
@InstallIn(ViewModelComponent::class)
abstract class PcAndroidViewModelModule {

    @Binds
    @ViewModelScoped
    abstract fun bindDiscoveryRepository(
        discoveryRepository: DiscoveryRepositoryImpl
    ): DiscoveryRepository

}

@Module
@InstallIn(SingletonComponent::class)
class PcAndroidServiceModule {

    @Singleton
    @Provides
    fun bindConnectionService(
        @ApplicationContext appContext: Context
    ): ConnectionRepository {
        return ConnectionRepositoryImpl(appContext)
    }

}

@EntryPoint
@InstallIn(SingletonComponent::class)
interface CommonEntryPoint {
    fun connectionRepository(): ConnectionRepository
}